#ifndef STYLEWIDGET_H
#define STYLEWIDGET_H

#include <QObject>
#include <QCheckBox>
#include <QLineEdit>
#include <QComboBox>
#include <QHBoxLayout>

class StyleWidget: public QWidget
{
    Q_OBJECT
public:
    StyleWidget(QWidget *parent=nullptr)
        : QWidget(parent),
          nameEditor(new QLineEdit),
          valueEditor(new QLineEdit)
    {
        QHBoxLayout *layout = new QHBoxLayout();
        layout->addWidget(nameEditor);
        layout->addWidget(valueEditor);

        setLayout(layout);
    }

    QString value(){
        return valueEditor->text();
    }
    void setValue(const QString& text){
        valueEditor->setText(text);
    }
    QString name() const{
        return  nameEditor->text();
    }
    void setName(const QString &text){
        nameEditor->setText(text);
    }

    QSize sizeHint() const override
    {
        return layout()->sizeHint();
    }
private:
    QLineEdit *nameEditor;
    QLineEdit *valueEditor;
};

#endif // STYLEWIDGET_H
