#ifndef STYLEDELEGATE_H
#define STYLEDELEGATE_H

#include <QDebug>

#include <QStyledItemDelegate>
#include <QLineEdit>
#include <QFrame>

#include <QHBoxLayout>


#include "src/Widgets/StyleWidget.h"
#include "src/Models/AttributeModel.h"


class StyleDelegate: public QStyledItemDelegate{
    Q_OBJECT
public:
    using QStyledItemDelegate::QStyledItemDelegate;
//    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const{
//        Q_UNUSED(painter)
//        Q_UNUSED(option)
//        Q_UNUSED(index)
//    }
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const{
        Q_UNUSED(option)
        Q_UNUSED(index)
        StyleWidget *editor = new StyleWidget(parent);
        return editor;
    }
    void setEditorData(QWidget *editor, const QModelIndex &index) const{
        auto data = index.data(StyleModel::Roles::Record).toStringList();
        StyleWidget *widget = static_cast<StyleWidget*>(editor);

        widget->setName(data.takeFirst());
        widget->setValue(data.takeFirst());
    }
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {

        StyleWidget *widget = static_cast<StyleWidget*>(editor);
        StyleModel* m = static_cast<StyleModel*>(model);

        QStringList values {widget->name(), widget->value()};
        model->setData(index, QVariant(values), StyleModel::Roles::Record);
        model->setData(index, widget->sizeHint(), Qt::SizeHintRole);
    }

    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const{
        Q_UNUSED(index)
        editor->setGeometry(option.rect);
    }

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        QSize s =  index.data(Qt::SizeHintRole).toSize();



        // Find correct way to return editor size
        return QSize(60, 50);

        return s.isValid() ? s: QStyledItemDelegate::sizeHint(option, index);
    }
};


#endif // STYLEDELEGATE_H
